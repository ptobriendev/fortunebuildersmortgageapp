﻿using System;
using System.Web;
using System.Web.UI;

namespace MortgageCalculatorWebForm
{

    public partial class Default : System.Web.UI.Page
    {

        //This method runs the calcBorrowAmount() from objective 1.
        public void RunTotalAmountCalculations(object sender, EventArgs args)
        {

            var downpayment = double.Parse(dpInput.Text);
            var monthlyPayment = double.Parse(mpInput.Text);
            var customPercentage = customPercentInput.Text;
            var percentage = 0.00;

            if (percentInput.Visible)
            {
                percentage = double.Parse(percentInput.SelectedItem.Text);
            }
            else
            {
                percentage = double.Parse(customPercentInput.Text);
            }



            var result = MortgageCalculator.MortgageCalculator.calcBorrowAmt(downpayment, monthlyPayment, percentage);

            label1.Text = "You may need to borrow up to $" + result.ToString("#.##") + ".";
        }

        //This method shows or hides the custom interest textbox (for total amount tool) depending on the user selection.
        public void CheckForCustomValueSelection(object sender, EventArgs args)
        {

            if (percentInput.Visible)
            {
                percentInput.Visible = false;
                customPercentInput.Visible = true;
            }
            else
            {
                percentInput.Visible = true;
                customPercentInput.Visible = false;
            }

        }


        //This method runs the calcMonthlyPaymentAmount() from objective 1.
        public void RunMonthlyPaymentCalculations(object sender, EventArgs args)
        {

            var totalAmtBorrowed = double.Parse(totalLoanInput.Text);
            var downpayment = double.Parse(downPaymentInput.Text);
            var mortgageTerm = int.Parse(loanTermInput.Text);
            var customPercentage = CustomInterestInput.Text;
            var percentage = 0.00;

            if (mpPercentInput.Visible)
            {
                percentage = double.Parse(mpPercentInput.SelectedItem.Text);
            }
            else
            {
                percentage = double.Parse(CustomInterestInput.Text);
            }

            var result = MortgageCalculator.MortgageCalculator.calcMonthlyPaymentAmount(totalAmtBorrowed, downpayment, mortgageTerm, percentage);

            label2.Text = "The monthly payments will be $" + result.ToString("#.##") + ".";
        }

        //This method shows or hides the custom interest textbox (for total amount tool) depending on the user selection.
        public void ToggleCustomInterestBox(object sender, EventArgs args)
        {

            if (mpPercentInput.Visible)
            {
                mpPercentInput.Visible = false;
                CustomInterestInput.Visible = true;
            }
            else
            {
                mpPercentInput.Visible = true;
                CustomInterestInput.Visible = false;
            }

        }

    }
}
