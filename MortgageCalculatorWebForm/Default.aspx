﻿<%@ Page Language="C#" Inherits="MortgageCalculatorWebForm.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Mortgage Calculator</title>
</head>
<body>
        <h1>Mortgage Calculator</h1>
        
        <h3>Use this tool to help determine the total amount you may need to borrow.</h3>
       
        <!-- Form to help the user determine how much they should borrow -->
    <form id="form1" runat="server">
        Down payment amount: <asp:TextBox id = "dpInput" runat="server"/>  <br>
        Monthly payment amount: <asp:TextBox id = "mpInput" runat="server"/>  <br>
            
         <!-- determine if the user would like to use the predefined list of interest rates or enter one themselves -->   
        Would you like to enter a custom interest percentage? 
        <asp:CheckBox ID="totalAmountCB" runat="server" oncheckedchanged="CheckForCustomValueSelection" AutoPostBack="true" /><br>
            
        <!-- dropdown list of predefined options -->
        Interest percentage: <asp:dropdownlist id="percentInput" runat="server">  
             <asp:listitem text="1.15" value="1"></asp:listitem>
             <asp:listitem text="3.75" value="2"></asp:listitem>
             <asp:listitem text="4.125" value="3"></asp:listitem>
             <asp:listitem text="4.35" value="4"></asp:listitem>
        </asp:dropdownlist>
            
           <!-- textbox for custom value -->
          <asp:TextBox id="customPercentInput" runat="server" Visible="false" />
            
		<asp:Button id="button1" runat="server" Text="Submit" OnClick="RunTotalAmountCalculations" /><br>
       <h2> <asp:Label id="label1" runat="server" /></h2>
	</form><br>
        <!-- End Form 1 -->
        
        
        
        <!-- Form to help the user determine how the monthly payment amount -->
    <h3>Use this tool to help determine the monthly payment amount.</h3>
        
   <form id="form2" runat="server">
        Loan amount <asp:TextBox id = "totalLoanInput" runat="server" /> <br>
        Down payment amount <asp:TextBox id = "downPaymentInput" runat="server" /> <br>
        Loan term <asp:TextBox id = "loanTermInput" runat="server" /> <br>    
            
        <!-- dropdown list of predefined options -->
        Interest percentage: <asp:dropdownlist id="mpPercentInput" runat="server">  
             <asp:listitem text="1.15" value="1"></asp:listitem>
             <asp:listitem text="3.75" value="2"></asp:listitem>
             <asp:listitem text="4.125" value="3"></asp:listitem>
             <asp:listitem text="4.35" value="4"></asp:listitem>
            </asp:dropdownlist>
            
          <!-- textbox for custom value -->
          <asp:TextBox id="CustomInterestInput" runat="server" Visible="false"/>
            
         <!-- determine if the user would like to use the predefined list of interest rates or enter one themselves -->   
        <div><asp:button id="mpInterestButton" runat="server" Text="Enter custom interest percentage" OnClick="ToggleCustomInterestBox"/>   
        <asp:Button id="button2" runat="server" Text="Submit" OnClick="RunMonthlyPaymentCalculations" /></div>
       <h2> <asp:Label id="label2" runat="server" /></h2>
 </form>
        <!-- End form 2 -->
</body>
</html>
