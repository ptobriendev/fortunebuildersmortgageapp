using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MortgageCalculatorTest
{
    [TestClass]
    public class MortgageCalcTest
    {

        //BEGIN TESTS FOR calcBorrowAmt()
        //This test verifies that the method is returning something.
        [TestMethod]
        public void TestCalBorrowAmtReturn()
        {
            //instantiates each variable in CalcBorrowAmt()'s signature.
            var downpayment = 500.00;
            var monthlyPayment = 3000.00;
            var percentage = 0.15;

            var result = MortgageCalculator.MortgageCalculator.calcBorrowAmt(downpayment, monthlyPayment, percentage);

            Assert.IsNotNull(result);

        }

        //This test verifies that the returned result is calculating as expected.
        [TestMethod]
        public void TestCalBorrowAmtCalculations()
        {
            //instantiates each variable in CalcBorrowAmt()'s signature.
            var downpayment = 1000.00;
            var monthlyPayment = 3000.00;
            var percentage = 1.15;

            //This is the result we expect to be returned.
            var expectedResult = 4600.00;

            var result = MortgageCalculator.MortgageCalculator.calcBorrowAmt(downpayment, monthlyPayment, percentage);

            Assert.AreEqual(expectedResult, result);

        }
        //END TESTS FOR calcBorrowAmt()

        //BEGIN TESTS FOR calcMonthlyPaymentAmount()
        //This test verifies that the method is returning something.
        [TestMethod]
        public void TestcalcMonthlyPaymentAmountReturn()
        {

            var totalAmtBorrowed = 30000.00;
            var downpayment = 5000.00;
            var mortgageTerm = 30;
            var percentage = 1.15;

            var result = MortgageCalculator.MortgageCalculator.calcMonthlyPaymentAmount(totalAmtBorrowed, downpayment, mortgageTerm, percentage);

            Assert.IsNotNull(result);

        }

        //This test verifies that the returned result is calculating as expected.
        [TestMethod]
        public void TestcalcMonthlyPaymentAmountCalculations()
        {
            //instantiates each variable in CalcBorrowAmt()'s signature.
            var totalAmtBorrowed = 30000.00;
            var downpayment = 4500.00;
            var mortgageTerm = 30;
            var percentage = 1.15;

            //This is the result we expect to be returned.
            var expectedResult = 1000.00;

            var result = MortgageCalculator.MortgageCalculator.calcMonthlyPaymentAmount(totalAmtBorrowed, downpayment, mortgageTerm, percentage);

            Assert.AreEqual(expectedResult, result);

        }
        //END TESTS FOR calcMonthlyPaymentAmount()

    }
}
