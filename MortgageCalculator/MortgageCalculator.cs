﻿using System;

namespace MortgageCalculator
{
    public class MortgageCalculator
    {
        //This method calculates monthly payments, downpayment, and percentage values and returns the sum. 
        //This will help portential borrowers determine how much they should apply to borrow.
        public static double calcBorrowAmt(double downPayment, double monthlyPayment, double percentage)
        {
            var borrowAmt = (downPayment + monthlyPayment) * percentage;

            return borrowAmt;
        }

        //This method calculates monthly payments based on total amount, downpayment, mortgage term and percentage
        public static double calcMonthlyPaymentAmount(double totalAmtBorrowed, double downpayment, double mortgageTerm, double percentage)
        {
            var monthlyPaymentAmt = ((totalAmtBorrowed * percentage) - downpayment) / mortgageTerm;
            return monthlyPaymentAmt;
        }
    }
}
